# Unity3D canvas rendertotexture bug 

if you have a bug with alphas getting screwed up when you render Unity3d UI to render texture.  Please note that this is BY design.  However, here is a workaround. 


You need to add the following line to the UI shaders you are using.  

BlendOp Add, Max

this prevents the shader from writing putting in unexpeteced alpha into the render texture on the edges of text.   If you put the file into your project, unity should use your local version of the default shader instead of its version.   Make sure it's included in the build, by placing it in a resource folder or linking it to the project somehow.
